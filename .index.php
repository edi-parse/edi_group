<?php

$serverName = "192.168.1.252"; //serverName\instanceName
$connectionInfo = array( "Database"=>"FeitApps", "UID"=>"feitphp", "PWD"=>"4901Gregg");
$conn = sqlsrv_connect( $serverName, $connectionInfo);
date_default_timezone_set("America/Los_Angeles");
date_default_timezone_set("America/Los_Angeles");
$tsql = "SELECT *
        FROM [FeitApps].[dbo].[EDI_Document]
        
        ";    
    /* Execute the query. */    
    $stmt = sqlsrv_query( $conn, $tsql);    
    if ( $stmt )    
    {    
         echo "";    
    }     
    else     
    {    
         echo "Error in statement execution.\n";    
         die( print_r( sqlsrv_errors(), true));    
    }

    while( $row = sqlsrv_fetch_array( $stmt))  
    {
$enteredFiles[] = $row['documentFileName'];
    } 
?>

<!doctype html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="./.favicon.ico">
   <title>EDI-Group Directory Contents</title>
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="eightTwenty/typeahead.js"></script>
   <link rel="stylesheet" href="./.style.css">
   <script src="./.sorttable.js"></script>
   <script>
    window.onload = function() {
        (document.getElementsByTagName( 'th' )[4]).click();
         (document.getElementsByTagName( 'th' )[4]).click();
    };
</script>
</head>

<body>
<div id="container">
	<h1>EDI-Group Directory Contents</h1>
        <table style="text-align: center;">
            <tr style="background-color: white;">
                <td>
            
<form action="Chargebacks.php" method="GET">
    <label class="demo-label">Search Shortages:</label><br/> 
    <input type="text" style="width:180px;" name="checkNumber" id="checkNumber" class="typeahead" autocomplete="off"/>
    <input type="submit" value="View Check" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	
                </td>
                <td>
                   
<form action="CreditMemos.php" method="GET">
    <label class="demo-label">Search Checks:</label><br/> 
    <input type="text" style="width:180px;" name="checkNumber1" id="checkNumber1" class="typeahead" autocomplete="off"/>
    <input type="submit" value="View Check" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	 
                </td>
                <td>
                
<form action="RMA.php" method="GET">
    <label class="demo-label">Search RGAs:</label><br/> 
    <input type="text" style="width:180px;" name="RMA" id="RMA" autocomplete="off"/>
    <input type="submit" value="View RGA" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	    
                </td>
            </tr>
            <tr>
                <td>
                  
<form action="eightTwenty/820Checks.php" method="GET">
    <label class="demo-label">Payments and RTVs:</label><br/> 
    <input type="text" style="width:180px;" name="checkNumber2" id="checkNumber2" class="typeahead" autocomplete="off"/>
    <input type="submit" value="View Check" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	   
                </td>
                <td>
                 
<form action="RTV.php" method="GET">
    <label class="demo-label">Search RTVs:</label><br/> 
    <input type="text" style="width:180px;" name="RTV" id="RTV" autocomplete="off"/>
    <input type="submit" value="View RTV" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	    
                </td>
                <td>
             
<form action="Invoices.php" method="GET">
    <label class="demo-label">Search Invoices:</label><br/> 
    <input type="text" style="width:180px;" name="invoice" id="invoice" autocomplete="off"/>
    <input type="submit" value="View Invoice" style="padding: 5px;width:100px;font-size: 16px;"/>
</form>
	      
                </td>
            </tr>
        </table>

	<table class="sortable">
	    <thead>
		<tr>
                        <th>Imported</th>	
                        <th>Filename</th>
                        <th>Type</th>
			<th>Size</th>
			<th>Date Modified</th>
		</tr>
	    </thead>
	    <tbody>
        
        <?php

	// Adds pretty filesizes
	function pretty_filesize($file) {
		$size=filesize($file);
		if($size<1024){$size=$size." Bytes";}
		elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." KB";}
		elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." MB";}
		else{$size=round($size/1073741824, 1)." GB";}
		return $size;
	}

 	// Checks to see if veiwing hidden files is enabled
	if($_SERVER['QUERY_STRING']=="hidden")
	{$hide="";
	 $ahref="./";
	 $atext="Hide";}
	else
	{$hide=".";
	 $ahref="./?hidden";
	 $atext="Show";}
       
         // Opens directory
         $path = "edi_files/";
        
	 // Opens directory
	 $myDirectory=opendir($path);

	// Gets each entry
	while($entryName=readdir($myDirectory)) {
	   $dirArray[]=$entryName;
	}
$filesInDirectory = count($dirArray);
	// Closes directory
	closedir($myDirectory);

	// Counts elements in array
	$indexCount=count($dirArray);

	// Sorts files
	sort($dirArray);

	// Loops through the array of files
	for($index=0; $index < $indexCount; $index++) {

	// Decides if hidden files should be displayed, based on query above.
	    if(substr("$dirArray[$index]", 0, 1)!=$hide) {

	// Resets Variables
		$favicon="";
		$class="file";

	// Gets File Names
		$name=$path.$dirArray[$index];
		$namehref=$dirArray[$index];

	// Gets Date Modified
		$modtime=date("M j Y g:i A", filemtime($path.$dirArray[$index]));
		$timekey=date("YmdHis", filemtime($path.$dirArray[$index]));


	// Separates directories, and performs operations on those directories
		if(is_dir($dirArray[$index]))
		{
				$extn="&lt;Directory&gt;";
				$size="&lt;Directory&gt;";
				$sizekey="0";
				$class="dir";

			// Gets favicon.ico, and displays it, only if it exists.
				if(file_exists("$namehref/favicon.ico"))
					{
						$favicon=" style='background-image:url($namehref/favicon.ico);'";
						$extn="&lt;Website&gt;";
					}

			// Cleans up . and .. directories
				if($name=="."){$name=". (Current Directory)"; $extn="&lt;System Dir&gt;"; $favicon=" style='background-image:url($namehref/.favicon.ico);'";}
				if($name==".."){$name=".. (Parent Directory)"; $extn="&lt;System Dir&gt;";}
		}

	// File-only operations
		else{
			// Gets file extension
			$extn=pathinfo($dirArray[$index], PATHINFO_EXTENSION);

			// Prettifies file type
			switch ($extn){
				case "png": $extn="PNG Image"; break;
				case "jpg": $extn="JPEG Image"; break;
				case "jpeg": $extn="JPEG Image"; break;
				case "svg": $extn="SVG Image"; break;
				case "gif": $extn="GIF Image"; break;
				case "ico": $extn="Windows Icon"; break;

				case "txt": $extn="Text File"; break;
				case "log": $extn="Log File"; break;
				case "htm": $extn="HTML File"; break;
				case "html": $extn="HTML File"; break;
				case "xhtml": $extn="HTML File"; break;
				case "shtml": $extn="HTML File"; break;
				case "php": $extn="PHP Script"; break;
				case "js": $extn="Javascript File"; break;
				case "css": $extn="Stylesheet"; break;

				case "pdf": $extn="PDF Document"; break;
				case "xls": $extn="Spreadsheet"; break;
				case "xlsx": $extn="Spreadsheet"; break;
				case "doc": $extn="Microsoft Word Document"; break;
				case "docx": $extn="Microsoft Word Document"; break;

				case "zip": $extn="ZIP Archive"; break;
				case "htaccess": $extn="Apache Config File"; break;
				case "exe": $extn="Windows Executable"; break;

				default: if($extn!=""){$extn=strtoupper($extn)." File";} else{$extn="Unknown";} break;
			}

			// Gets and cleans up file size
				$size=pretty_filesize($path.$dirArray[$index]);
				$sizekey=filesize($path.$dirArray[$index]);
		}

	// Output
	 ?>
		<tr class='<?php echo $class?>'>
                        <td>
<?php 
$docType812 = '812';
$docType812Found = strpos($name, $docType812);
    IF ($docType812Found == true)
                    { 
    IF (in_array($namehref, $enteredFiles)) { ?>
                                <!--<a href='eightTwelve/viewReport.php' class='name'>Yes</a>-->
                                <a href='#' class='name'>Yes</a>
              <?php }
               else { ?>
                                <a href='eightTwelve/812.php?file=<?php echo $namehref?>' class='name'>No</a>
              <?php }    
                    } 
                    ?>
                                
<?php 
$docType820 = '820';
$docType820Found = strpos($name, $docType820);
    IF ($docType820Found == true)
                    { 
    IF (in_array($namehref, $enteredFiles)) { ?>
                               <!-- <a href='eightTwenty/searchCheck.php' class='name'>Yes</a>-->
                                <a href='#' class='name'>Yes</a>
              <?php }
               else { ?>
                                <a href='eightTwenty/820Raw.php?file=<?php echo $namehref?>' class='name'>No</a>
              <?php }    
                    } 
                    ?>
                        
                       
                        </td>  
			<td><?php echo $name?></td>
                        
                        
                        
                        
			<td><?php echo $extn?></td>
			<td sorttable_customkey='<?php echo $sizekey?>'><?php echo $size?></td>
			<td sorttable_customkey='<?php echo $timekey?>'><?php echo $modtime?></td>
		</tr>
                <?php
          
	   }
           
	}
	?>

	    </tbody>
	</table>
<script>
    $(document).ready(function () {
        $('#checkNumber').typeahead({
            source: function (query, result) {
                
                $.ajax({
                    url: "eightTwenty/server.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#checkNumber1').typeahead({
            source: function (query, result) {
                
                $.ajax({
                    url: "eightTwenty/server.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#checkNumber2').typeahead({
            source: function (query, result) {
                
                $.ajax({
                    url: "eightTwenty/server.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>
	<!--<h2><?php echo("<a href='$ahref'>$atext hidden files</a>"); ?></h2>-->
</div>
</body>
</html>
