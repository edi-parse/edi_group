<?php
$serverName = "192.168.1.252"; //serverName\instanceName
$connectionInfo = array( "Database"=>"FeitApps", "UID"=>"feitphp", "PWD"=>"4901Gregg");
$conn = sqlsrv_connect( $serverName, $connectionInfo);
date_default_timezone_set("America/Los_Angeles");
date_default_timezone_set("America/Los_Angeles");

?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/css/bootstrap-combobox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/js/bootstrap-combobox.min.js"></script>
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	
<script>

$(document).ready(function(){
  $('.combobox').combobox();
  
  // bonus: add a placeholder
  $('.combobox').attr('placeholder', 'For example, start typing "Home Depot"');
});</script> 
<style type="text/css">
    .breadcrumb {background-color:#FFF; font-weght:bold;}
    .well {background-color:#FFF;border: 1px solid #FFF;}
</style>
    <script type="text/javascript" class="init">
	$(document).ready(function() {
    var printCounter = 0;
 
    // Append a caption to the table before the DataTables initialisation
    $('#example').append('<caption style="caption-side: bottom"> &copy; 2018 Feit Electric</caption>');
 
    $('#example').DataTable( {
        
        "order": [[ 6, "desc" ]],
        "pageLength": 50,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copy',
                Title: 'Title goes here.',
               messageTop: 'Top message goes here.',
              messageBottom: 'Bottom message goes here.'
            },
            {
                extend: 'excel',
                messageTop: 'The information in this table is propert of Feit Electric.'
            },
            {
                extend: 'print',
                messageTop: function () {
                    printCounter++;
 
                    if ( printCounter === 1 ) {
                        return 'This is the first time you have printed this document.';
                    }
                    else {
                        return 'You have printed this document '+printCounter+' times';
                    }
                },
                messageBottom: null
            }
        ]
    } );
} );
        
    </script>
    <meta charset=utf-8 />
    <title>812 Invoice Report</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  $( function() {
    $( "#datepicker1" ).datepicker();
  } );
  </script>
  <style type="text/css">
      tr:nth-of-type(5n) {
    page-break-after:always;
}
  </style>
  </head>
  <body>

<div class="col-sm-12">
    <div class="well">
        <table id="example" class="display nowrap" width="100%">
            <thead>
                <tr>
                    <th>FILE</th>
                    <th>FORM</th>
                    <th>INVOICE #</th>
                    <th>STORE</th>
                    <th>INVOICE DATE</th>
                    <th>PO</th>
                    <th>P-VENDOR</th>
                    <th>ITEM</th>
                    <th>SKU</th>
                    <th>NAV ITEM</th>
                    <th>PRICE</th>
                    <th>QUANTITY</th>
                    <th>TOTAL</th>
            
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>FILE</th>
                    <th>FORM</th>
                    <th>INVOICE #</th>
                    <th>STORE</th>
                    <th>INVOICE DATE</th>
                    <th>PO</th>
                    <th>P-VENDOR</th>
                    <th>ITEM</th>
                    <th>SKU</th>
                    <th>NAV ITEM</th>
                    <th>PRICE</th>
                    <th>QUANTITY</th>
                    <th>TOTAL</th>
                </tr>
            </tfoot>
            <tbody>
<?php
$tsql = " 
SELECT *
  FROM [FeitApps].[dbo].[EDI812CBWPARTNO]
  WHERE EDI_812_CB_TYPE = 'A'
";    
/* Execute the query. */    
$stmt = sqlsrv_query( $conn, $tsql);    
if ( $stmt )    
{    
     echo "";    
}     
else     
{    
     echo "Error in statement execution.\n";    
     die( print_r( sqlsrv_errors(), true));    
}  

/* Iterate through the result set printing a row of data upon each iteration.*/    
while( $obj = sqlsrv_fetch_object( $stmt))  
{  
$EDI_812_CB_FILE = $obj->EDI_812_CB_FILE;
$EDI_812_CB_TYPE = $obj->EDI_812_CB_TYPE;
$EDI_812_CB_INVOICE = $obj->EDI_812_CB_INVOICE;
$EDI_812_CB_STORE = $obj->EDI_812_CB_STORE;
$EDI_812_CB_INVOICE_DATE = $obj->EDI_812_CB_INVOICE_DATE;
$EDI_812_CB_PO = $obj->EDI_812_CB_PO;
$EDI_812_CB_VR_ACCOUNT = $obj->EDI_812_CB_VR_ACCOUNT;
$EDI_812_CB_VEND_PART = $obj->EDI_812_CB_VEND_PART;
$EDI_812_CB_SKU = $obj->EDI_812_CB_SKU;
$NAV_ITEM = $obj->NAV_ITEM;
$EDI_812_CB_AMOUNT_CALC = $obj->EDI_812_CB_AMOUNT_CALC;
$EDI_812_CB_AMOUNT = $obj->EDI_812_CB_AMOUNT;

IF ($EDI_812_CB_TYPE == 'A'){$EDI_812_CB_TYPE = 'SHORTAGE';}
?>
                <tr>
                    <td><?php echo $EDI_812_CB_FILE;?></td>
                    <td><?php echo $EDI_812_CB_TYPE;?></td>
                    <td><?php echo $EDI_812_CB_INVOICE;?></td>
                    <td><?php echo substr($EDI_812_CB_STORE,2);?></td>
                    <td><?php echo $EDI_812_CB_INVOICE_DATE;?></td> 
                    <td><?php echo $EDI_812_CB_PO;?></td>
                    <td><?php echo $EDI_812_CB_VR_ACCOUNT;?></td>
                    <td><?php echo $EDI_812_CB_VEND_PART;?></td>
                    <td><?php echo $EDI_812_CB_SKU;?></td>
                    <td><?php echo $NAV_ITEM;?></td>
                    <td><?php echo $EDI_812_CB_AMOUNT_CALC;?></td> 
                    <td><?php echo number_format($EDI_812_CB_AMOUNT);?></td> 
                    <td><?php echo number_format($EDI_812_CB_AMOUNT_CALC*$EDI_812_CB_AMOUNT,2, '.', '');?></td> 
                </tr>
<?php
} 
?>
</tbody>
 </table>
    </div>
    </div>
</body>
</html>

