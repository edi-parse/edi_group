<!DOCTYPE html>
<html>
<head>
    <title>Search Checks</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="typeahead.js"></script>
		
</head>
<body>
	<div style=' width:500px;height:500px;position:absolute;left:50%;top:20%;transform:translate(-50%, -20%);text-align:center;'>
            <form action="820Checks.php" method="GET">
		<label class="demo-label">Search Checks:</label><br/> 
                <input type="text" name="checkNumber" id="checkNumber" class="typeahead" autocomplete="off"/>
                <input type="submit" value="View Check"/>
            </form>
	</div>
</body>
<script>
    $(document).ready(function () {
        $('#checkNumber').typeahead({
            source: function (query, result) {
                
                $.ajax({
                    url: "server.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>
</html>